<?php
// Documentation de l'API
$documentation=array(
    /* PRODUCTS=5 */
    'view=PRODUCTS'=>array(
      	'description'=>'Liste les DVDs',
      	'method'=>'GET',
      	'params'=>array('CATEGORY'=>array('type'=>'int', 'mandatory'=>false, 'description'=>'Filtre par catégorie de DVD')),
      	'return'=>array('type'=>'array[object]')
    ),
    /* add=CATEGORY&CATEGORYNAME=KEVIN&description=test */
    'add=CATEGORY'=>array(
    	'description'=>'Ajouter une nouvelle catégorie',
	    'method'=>'PUT/POST/GET',
	    'params'=>array('CATEGORYNAME'=>array('type'=>'string', 'mandatory'=>true, 'description'=>'Nom de la catégorie à créer')),
        'return'=>'boolean'
    ),
    /* delete=DELETE&from=CATEGORIES&where=CATEGORY&values=? */
    'delete=DELETE'=>array(
        'description'=>'supprimer un champ',
        'method'=>'GET',
        'params'=>array('DELETE'=>array('type'=>'string', 'mandatory'=>true)),
        'return'=>'boolean',
    ),
    /* update=UPDATE&from=CUSTOMERS&set=phone&values1=0123456789&where=CUSTOMERID&values2=20002 */
    'update=UPDATE'=>array(
        'description'=>'effectuer une mise ajour des données',
        'method'=>'GET',
        'params'=>array('UPDATE'=>array('type'=>'string', 'mandatory'=>true)),
        'return'=>'boolean',
    )
);

// Fonctions et classes générales
function header_json() {
  header('content-type:application/json; charset=utf-8');
  header('Access-Control-Allow-Origin:*');
}
function param($P, $name, $default=NULL) {
  return isset($P[$name])?$P[$name]:$default;
}
class db {
    static $db;
    static function init() {
        self::$db=new PDO("mysql:host=localhost;dbname=DS2;charset=utf8", "wordpress", "112246aB?");
    }
    static function select($from, $what='*', $where=NULL, $params=NULL, $limit='0,10', $order=NULL) {
      $order=$order?" ORDER BY $order":'';
      if($where)
        $sql="SELECT $what FROM $from WHERE $where LIMIT $limit$order;";
      else
        $sql="SELECT $what FROM $from LIMIT $limit$order;";
      $req=self::$db->prepare($sql);
      $ret=$req->execute($params?$params:array());
      //error_log($sql);
      return $ret?$req:$ret;
    }
    static function insert($into, $keys, $values) {
  	foreach($keys as $key) $val[]='?';
  	$sql="INSERT INTO $into (".implode($keys,',').") VALUES (".implode($val, ',').");";
  	$req=self::$db->prepare($sql);
  	if(!is_array($values)) $values=array($values);
  	if(!is_array($values[0])) $values=array($values);
  	$n=0;
  	foreach($values as $value)
  	  if($req->execute($value)) $n++;
  	error_log($sql);
  	return $n;
    }
    static function delete($from, $where, $values) {
        $sql = "DELETE FROM $from WHERE $where = :values";
        $req = self::$db->prepare($sql);
        $ret= $req->execute(array(
            ':values' => $values
        ));
        return $ret?$req:$ret;
    }
    static function update($from, $set, $values1, $where, $values2) {
        $sql = "UPDATE $from SET $set = :values1 WHERE $where = :values2";
        $req = self::$db->prepare($sql);
        $ret= $req->execute(array(
            ':values1' => $values1,
            ':values2' => $values2
        ));
        return $ret?$req:$ret;
    }
}

// Fonctions d'API
function view_PRODUCTS($CATEGORY, $limit='0,10', $order=NULL) {
  if($CATEGORY)
	$req=db::select("PRODUCTS NATURAL JOIN CATEGORIES", "*", "CATEGORIES.CATEGORY=:CATEGORY", array(':CATEGORY'=>$CATEGORY), $limit);
  else
	$req=db::select("PRODUCTS NATURAL JOIN CATEGORIES", "*", NULL, NULL, $limit);
  if($req)
	return $req->fetchAll(PDO::FETCH_ASSOC);
  else return false;
}
function add_category($categoryname) {
  if(!$categoryname) return False;
  return !!db::insert('CATEGORIES', array('CATEGORYNAME'), $categoryname);
}
function deleteTable($from, $where, $values) {
    if(!$from && !$where && !$values) return 'vide!!!';
    return !!db::delete($from, $where, $values);
}
function updateTable($from, $set, $values1, $where, $values2) {
    if(!$from && !$set && !$values1 && !$where && !$values2) return 'vide!!!';
    return !!db::update($from, $set, $values1, $where, $values2);
}

/****************************
 * Traitement de la requête *
 ****************************/

// Paramètres généraux des requêtes
$METHOD=$_SERVER['REQUEST_METHOD'];
$LIMIT=param($_REQUEST, 'limit', '0,10');
$ORDER=param($_REQUEST, 'order', '0,10');
db::init();

// Routage
$action=NULL;
if($METHOD=='GET' && count($_GET)==0)
    $action='documentation';
elseif($METHOD =='GET' && isset($_GET['view']) && $_GET['view']=='PRODUCTS')
    $action='view_products';
elseif(isset($_REQUEST['add']) && $_REQUEST['add']=='CATEGORY')
    $action='add_category';
elseif($METHOD =='GET' && isset($_GET['delete']) && $_GET['delete']=='DELETE')
    $action='deleteTable';
elseif($METHOD =='GET' && isset($_GET['update']) && $_GET['update']=='UPDATE')
    $action='updateTable';

// Et action !
header_json();
switch($action) {
case 'documentation':
    echo json_encode($documentation);
    break;;
case 'add_category':
    echo json_encode(add_category(param($_REQUEST, 'CATEGORYNAME')));
    break;;
case 'view_products':
default:
    echo json_encode(view_products(param($_GET, 'CATEGORY'), $LIMIT, $ORDER));
    break;;
case 'deleteTable':
    echo json_encode(deleteTable($_GET['from'],$_GET['where'],$_GET['values']));
    break;;
case 'updateTable':
    echo json_encode(updateTable($_GET['from'],$_GET['set'],$_GET['values1'],$_GET['where'],$_GET['values2']));
}
