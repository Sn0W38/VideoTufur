//fonction view
$('#dvd').click(function(){
$.ajax ({
        url: 'PHP/api.php',
        dataType:'json',
        data:'view=PRODUCTS',
        method :  'GET'
    }).done(function(data) {
        for(var value in data) {
            $("#info").append(
                "<ul class='col-4 list-group mt-2 pr-1 pl-1'><li class='list-group-item list-group-item-primary text-center'>DVD n°" + value + ' : ' + '</li>' +
                '<li class="list-group-item"> Catégorie : ' + data[value].CATEGORYNAME + '</li><li class="list-group-item"> Titre : ' + data[value].TITLE + '</li><li class="list-group-item"> Acteur : ' + data[value].ACTOR +'</li><li class="list-group-item"> prix : '+ data[value].PRICE + "</li></ul>")
        }
    })
});

//fonction rest
$('#reset').click(function(){
	$("#info").empty()
});
//fonction add_category
$('#add').click(function(){
	var category = document.getElementById('category').value;
	$.ajax ({
        url: 'PHP/api.php',
        dataType:'json',
        data:'add=CATEGORY&CATEGORYNAME=' + category ,
        method :  'GET'
    }).done(function(data) {
        $("#info").empty()
        $('#info').append("<p class='text-primary'>Catégorie " + category + " a était ajouter.</p>")
    })
});
//fonction update
$('#update').click(function(){
    var UPDfrom = document.getElementById('UPDfrom').value;
    var UPDset = document.getElementById('UPDset').value;
    var UPDvalues1 = document.getElementById('UPDvalues1').value;
    var UPDwhere = document.getElementById('UPDwhere').value;
    var UPDvalues2 = document.getElementById('UPDvalues2').value;
$.ajax ({
        url: 'PHP/api.php',
        dataType:'json',
        data: 'update=UPDATE&from=' + UPDfrom + '&set=' + UPDset + '&values1=' + UPDvalues1 + '&where=' + UPDwhere  + '&values2=' + UPDvalues2,
        method :  'GET'
    }).done(function(data) {
        $("#info").empty()
        $('#info').append("<p class='text-primary'>Le champ à était modifier avec succes.</p>")
    })
});
//fonction delete
//api.php?delete=DELETE&from=CATEGORIES&where=CATEGORY&values=43
$('#delete').click(function(){
    var DELfrom = document.getElementById('DELfrom').value;
    var DELwhere = document.getElementById('DELwhere').value;
    var DELvalues = document.getElementById('DELvalues').value;
$.ajax ({
        url: 'PHP/api.php',
        dataType:'json',
        data: 'delete=DELETE&from=' + DELfrom + '&where=' + DELwhere + '&values=' + DELvalues,
        method :  'GET'
    }).done(function(data) {
        $("#info").empty()
        $('#info').append("<p class='text-primary'>Le champ à était supprimer avec succes.</p>")
    })
});
